package transcoddy

import (
	"context"
	"fmt"
	"io"
	"math"

	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type Transcoder interface {
	Transcode(ctx context.Context, t *Profile, hook ...io.Writer) error
}

type ProgressSession interface {
	GetProgressChannel() (<-chan *Progress, error)
}

type ProgressTranscoder struct {
	transcoder Transcoder
}

func NewProgressTranscoder(transcoder Transcoder) *ProgressTranscoder {
	return &ProgressTranscoder{
		transcoder: transcoder,
	}
}

func (pt *ProgressTranscoder) Transcode(ctx context.Context, sess ProgressSession, profile *Profile) error {
	durationHook, err := NewTotalDurationHook()
	if err != nil {
		return errors.Wrap(err, "new total duration hook")
	}

	transcoding, ctx := errgroup.WithContext(ctx)
	transcoding.Go(func() error {
		if err := pt.transcoder.Transcode(ctx, profile, durationHook); err != nil {
			return errors.Wrap(err, "run transcoder")
		}

		return nil
	})

	transcoding.Go(func() error {
		progress, err := sess.GetProgressChannel()
		if err != nil {
			return errors.Wrap(err, "get progress channel")
		}

		totalDuration, err := durationHook.GetTotalDuration(ctx)
		if err != nil {
			return errors.Wrap(err, "get total duration")
		}

		if totalDuration == 0 {
			return nil
		}

		for {
			select {
			case <-ctx.Done():
				return nil
			case p, ok := <-progress:
				if !ok {
					return nil
				}

				percentage := (float64(p.OutTime) / float64(totalDuration)) * 100
				fmt.Println(fmt.Sprintf("%.0f%%", math.Round(percentage)))
			}
		}

		return nil
	})

	if err := transcoding.Wait(); err != nil {
		return err
	}

	return nil
}
