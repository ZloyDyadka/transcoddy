package main

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/ZloyDyadka/transcoddy"
	"golang.org/x/sync/errgroup"
)

const (
	DefaultBindAddr  = "localhost:8080"
	DefaultRedisAddr = "localhost:6379"
	DefaultQueueName = "1488"
)

var (
	defaultLogger = log.Logger
)

func main() {
	if err := Run(); err != nil {
		defaultLogger.Fatal().Err(err).Msg("run application")
	}
}

func Run() error {
	logger := defaultLogger

	ctx := context.Background()
	ctx = logger.WithContext(ctx)

	redisClient := redis.NewClient(&redis.Options{
		Addr: DefaultRedisAddr,
	})

	progresser := transcoddy.NewHTTPFFmpegProgresserReceiver(transcoddy.NewProgressParser())
	ffmpeg := transcoddy.NewFFMPEG("ffmpeg")

	mux := http.NewServeMux()
	mux.HandleFunc("/", progresser.ProgressReceiverHandler)

	server := http.Server{
		Addr:    DefaultBindAddr,
		Handler: mux,
	}

	progressTranscoder := transcoddy.NewProgressTranscoder(ffmpeg)

	taskProcessor := &TaskProcessor{
		Logger:      logger,
		queue:       DefaultQueueName,
		transcoder:  progressTranscoder,
		serverAddr:  server.Addr,
		progresser:  progresser,
		redisClient: redisClient,
	}

	runner, ctx := errgroup.WithContext(ctx)
	runner.Go(func() error {
		if err := taskProcessor.Process(ctx); err != nil {
			return errors.Wrap(err, "process tasks")
		}

		return nil
	})

	runner.Go(func() error {
		if err := server.ListenAndServe(); err != nil {
			return errors.Wrap(err, "listen and seve progress pusher API")
		}

		return nil
	})

	runner.Go(func() error {
		<-ctx.Done()

		ctx, _ = context.WithTimeout(context.Background(), time.Second*5)
		if err := server.Shutdown(ctx); err != nil {
			return errors.Wrap(err, "shutdown HTTP server")
		}

		return nil
	})

	if err := runner.Wait(); err != nil {
		return err
	}

	return nil
}

type TaskProcessor struct {
	redisClient *redis.Client
	transcoder  *transcoddy.ProgressTranscoder
	progresser  *transcoddy.HTTPFFmpegProgresserReceiver
	queue       string
	serverAddr  string
	Logger      zerolog.Logger
}

var (
	ErrNoTasks = errors.New("no tasks")
)

func (tr *TaskProcessor) Process(ctx context.Context) error {
	logger := tr.Logger

	tr.redisClient.RPush(tr.queue, "-loglevel verbose -hide_banner -i big-buck-bunny_trailer.webm -preset veryslow -c:v libx264 -f null /dev/null")

	logger = logger.With().
		Str("queue_name", tr.queue).
		Logger()

	ctx = logger.WithContext(ctx)
	for {
		if err := tr.getAndRunTask(ctx); err != nil {
			causeErr := errors.Cause(err)
			switch causeErr {
			case ErrNoTasks:
				logger.Debug().Msg("no tasks")
			default:
				return errors.Wrap(err, "get and run task")
			}
		}
	}
}

func (tr *TaskProcessor) getAndRunTask(ctx context.Context) error {
	logger := tr.Logger

	logger.Debug().Msg("pop task from right and push to left queue")
	rawTask, err := tr.redisClient.BRPopLPush(tr.queue, tr.queue, time.Second*5).Result() // at-least-once
	if err != nil {
		if err == redis.Nil {
			return ErrNoTasks
		}

		return errors.Wrap(err, "pop and push task from queue")
	}

	logger.Debug().Str("raw_task", rawTask).Msg("parse task")
	task, err := transcoddy.ParseProfile(rawTask)
	if err != nil {
		return errors.Wrap(err, "parse task")
	}

	logger = logger.With().
		Str("task", task.String()).
		Logger()
	ctx = logger.WithContext(ctx)

	logger.Info().Msg("run task")
	if err := tr.runTask(ctx, task); err != nil {
		return errors.Wrap(err, "process task")
	}

	_, err = tr.redisClient.LPop(tr.queue).Result()
	if err != nil {
		return errors.Wrap(err, "pop task from queue")
	}

	return nil
}

func (tr *TaskProcessor) runTask(ctx context.Context, profile *transcoddy.Profile) error {
	logger := *zerolog.Ctx(ctx)

	sid := strconv.FormatInt(time.Now().UnixNano(), 10)
	logger = logger.With().
		Str("sid", sid).
		Logger()
	ctx = logger.WithContext(ctx)

	receiver, err := transcoddy.BuildReceiverURL(tr.serverAddr, sid)
	if err != nil {
		return errors.Wrap(err, "build receiver url")
	}
	profile.AddParam("-progress", receiver.String())

	sess, ok := tr.progresser.LoadOrCreateSession(sid)
	if ok {
		return errors.New("session is already exists")
	}
	defer func() {
	    sess.Close()
    	if err := tr.progresser.RemoveSession(sid); err != nil {
    		logger.Error().Err(err).Msg("remove session")
    	}
	}()

	logger.Info().Str("profile", profile.String()).Msg("run transcode")

	if err := tr.transcoder.Transcode(ctx, sess, profile); err != nil {
		return errors.Wrap(err, "transcode")
	}

	return nil
}
