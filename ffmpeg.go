package transcoddy

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

type ErrFFmpeg struct {
	Stderr string
	CmdErr error
	Cmd    string
}

func (e ErrFFmpeg) Error() string {
	return fmt.Sprintf("%s, profile: %s, log from stderr `%s`", e.CmdErr, e.Cmd, e.Stderr)
}

type FFMPEG struct {
	bin         string
	stderrHooks []io.Writer
}

func NewFFMPEG(bin string) *FFMPEG {
	return &FFMPEG{
		bin: bin,
	}
}

func (f *FFMPEG) Transcode(ctx context.Context, t *Profile, stderrHooks ...io.Writer) error {
	logger := zerolog.Ctx(ctx)

	cmd := exec.CommandContext(ctx, f.bin, t.args...)

	buf := bytes.NewBuffer(nil)
	cmd.Stderr = io.MultiWriter(append(stderrHooks, buf)...)

	logger.Debug().Msg("start ffmpeg")
	if err := cmd.Run(); nil != err {
		ferr := ErrFFmpeg{
			Stderr: buf.String(),
			CmdErr: err,
			Cmd:    t.String(),
		}

		return errors.Wrap(ferr, "run binary")
	}

	return nil
}

type TotalDurationHook struct {
	buf            *bytes.Buffer
	ready          chan struct{}
	totalDuration  time.Duration
	durationRegexp *regexp.Regexp
	finish         bool
}

func NewTotalDurationHook() (*TotalDurationHook, error) {
	rgxp, err := regexp.Compile(`Duration:\s*([0-9.\:]+)`)
	if err != nil {
		return nil, errors.Wrap(err, "compile regexp for duration")
	}

	return &TotalDurationHook{
		ready:          make(chan struct{}),
		buf:            bytes.NewBuffer(nil),
		durationRegexp: rgxp,
	}, nil
}

func (dh *TotalDurationHook) GetTotalDuration(ctx context.Context) (time.Duration, error) {
	select {
	case <-ctx.Done():
		return 0, ctx.Err()
	case <-dh.ready:
	}

	return dh.totalDuration, nil
}

func (dh *TotalDurationHook) Write(b []byte) (int, error) {
	if dh.finish {
		return len(b), nil
	}

	n, err := dh.buf.Write(b)
	if err != nil {
		return 0, errors.Wrap(err, "write data to buffer")
	}

	rd, err := regexp.Compile(`Duration:\s*([0-9.\:]+)`)
	if err != nil {
		return 0, err
	}

	dur := rd.FindSubmatch(dh.buf.Bytes())
	if len(dur) != 0 {
		totalDuration, err := ParseFFmpegDuration(string(dur[1]))
		if err != nil {
			return 0, errors.Wrap(err, "parse duration")
		}

		dh.totalDuration = totalDuration
		dh.finish = true
		close(dh.ready)
	}

	return n, nil
}

type Progress struct {
	Bitrate       int64         `json:"bitrate"`
	TotalSize     int64         `json:"total_size"`
	Speed         float64       `json:"speed"`
	Size          uint64        `json:"size"`
	FPS           float64       `json:"fps"`
	Frame         int64         `json:"frame"`
	Quantizer     int32         `json:"quantizer"`
	Drop          int32         `json:"drop"`
	OutTime       time.Duration `json:"out_time"`
	ProgressStage string        `json:"progress_stage"`
}

type ProgressParser interface {
	NextProgress(ctx context.Context, r *bufio.Reader) (*Progress, error)
}

type HTTPFFmpegProgresserReceiver struct {
	sessions map[string]*HTTPProgressReceiverSession
	parser   ProgressParser
	closed   bool
	l        sync.RWMutex
	Logger   zerolog.Logger
}

type HTTPProgressReceiverSession struct {
	progress chan *Progress
	closed   bool
	closing  chan struct{}
	l        sync.RWMutex
}

var (
	ErrSessionClosed = errors.New("session is closed")
)

func (s *HTTPProgressReceiverSession) SendProgress(ctx context.Context, p *Progress) error {
	if s.IsClosed() {
		return ErrSessionClosed
	}

	select {
	case <-s.closing:
		return ErrSessionClosed
	case s.progress <- p:
	case <-ctx.Done():
		return ctx.Err()
	}

	return nil
}

func (s *HTTPProgressReceiverSession) GetProgressChannel() (<-chan *Progress, error) {
	if s.IsClosed() {
		return nil, ErrSessionClosed
	}

	return s.progress, nil
}

func (s *HTTPProgressReceiverSession) Close() error {
	s.l.Lock()
	defer s.l.Unlock()

	if s.closed {
		return nil
	}

	close(s.closing)
	close(s.progress)

	s.closed = true

	return nil
}

func (s *HTTPProgressReceiverSession) IsClosed() bool {
	s.l.RLock()
	defer s.l.RUnlock()

	return s.closed
}

func NewHTTPFFmpegProgresserReceiver(parser ProgressParser) *HTTPFFmpegProgresserReceiver {
	pl := &HTTPFFmpegProgresserReceiver{
		sessions: make(map[string]*HTTPProgressReceiverSession),
		parser:   parser,
	}

	return pl
}

func (fp *HTTPFFmpegProgresserReceiver) isClosed() bool {
	fp.l.RLock()
	defer fp.l.RUnlock()

	return fp.closed
}

var (
	ErrInvalidSessionID = errors.New("invalid session id")
)

func (fp *HTTPFFmpegProgresserReceiver) ProgressReceiverHandler(w http.ResponseWriter, r *http.Request) {
	if err := fp.progressHandler(w, r); err != nil {
		fp.Logger.Error().Err(err).Msg("handle progress pusher")

		//фымпег все равно ошибки даже код ошибки полученный не печатает, поэтому бестолоково ему отдавать подробную ошибку.
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func (fp *HTTPFFmpegProgresserReceiver) progressHandler(w http.ResponseWriter, r *http.Request) error {
	sessionID := r.URL.Query().Get("sid")
	if sessionID == "" {
		return ErrInvalidSessionID
	}

	session, ok := fp.GetSession(sessionID)
	if !ok {
		return errors.New("session not found")
	}
	defer session.Close()

	reader := bufio.NewReader(r.Body)
	for {
		progress, err := fp.parser.NextProgress(context.Background(), reader)
		if err != nil {
			causeErr := errors.Cause(err)

			switch causeErr {
			case io.EOF:
				return nil
			default:
				return errors.Wrap(err, "get next progress status")
			}
		}

		if err := session.SendProgress(context.Background(), progress); err != nil {
			return errors.Wrap(err, "send progress to consumer")
		}
	}
}

func (fp *HTTPFFmpegProgresserReceiver) GetSession(key string) (*HTTPProgressReceiverSession, bool) {
	fp.l.RLock()
	defer fp.l.RUnlock()

	o, ok := fp.sessions[key]
	return o, ok
}

func (fp *HTTPFFmpegProgresserReceiver) LoadOrCreateSession(key string) (*HTTPProgressReceiverSession, bool) {
	fp.l.Lock()
	defer fp.l.Unlock()

	session, loaded := fp.sessions[key]
	if !loaded {
		session = &HTTPProgressReceiverSession{
			closing:  make(chan struct{}),
			progress: make(chan *Progress),
		}
		fp.sessions[key] = session
	}

	return session, loaded
}

func (fp *HTTPFFmpegProgresserReceiver) RemoveSession(key string) error {
	fp.l.Lock()
	defer fp.l.Unlock()

	delete(fp.sessions, key)

	return nil
}

func (fp *HTTPFFmpegProgresserReceiver) Close() error {
	fp.l.Lock()
	defer fp.l.Unlock()

	if fp.closed {
		return nil
	}

	for _, s := range fp.sessions {
		if err := s.Close(); err != nil {
			return errors.Wrap(err, "close all sessions")
		}
	}

	fp.closed = true

	return nil
}

const (
	ProgressFrame           = "frame"
	ProgressFPS             = "fps"
	ProgressBitrate         = "bitrate"
	ProgressTotalSize       = "total_size"
	ProgressOutTimeUs       = "out_time_us"
	ProgressOutTimeMs       = "out_time_ms"
	ProgressOutTime         = "out_time"
	ProgressDuplicateFrames = "dup_frames"
	ProgressDropFrames      = "drop_frames"
	ProgressSpeed           = "speed"
	ProgressStatus          = "progress"
	ProgressDelimiter       = ProgressStatus
)

var ProgressFields = map[string]struct{}{
	ProgressFrame:           struct{}{},
	ProgressBitrate:         struct{}{},
	ProgressFPS:             struct{}{},
	ProgressTotalSize:       struct{}{},
	ProgressOutTimeUs:       struct{}{},
	ProgressOutTimeMs:       struct{}{},
	ProgressOutTime:         struct{}{},
	ProgressDuplicateFrames: struct{}{},
	ProgressDropFrames:      struct{}{},
	ProgressStatus:          struct{}{},
	ProgressSpeed:           struct{}{},
}

type progressParser struct {
}

func NewProgressParser() *progressParser {
	return &progressParser{}
}

func (pp *progressParser) NextProgress(ctx context.Context, r *bufio.Reader) (*Progress, error) {
	lines := make([][]byte, 0, len(ProgressFields))

	for {
		line, _, err := r.ReadLine()
		if err != nil {
			switch err {
			case io.EOF:
				progress, err := ParseProgress(lines)
				if err != nil {
					return nil, errors.Wrap(err, "parse progress")
				}

				return &progress, io.EOF
			default:
				return nil, errors.Wrap(err, "read line from source data")
			}
		}

		if len(line) == 0 {
			continue
		}

		row := strings.Split(string(line), "=")
		key := row[0]

		if _, ok := ProgressFields[key]; !ok {
			continue
		}

		lines = append(lines, line)

		if key != ProgressDelimiter {
			continue
		}

		progress, err := ParseProgress(lines)
		if err != nil {
			return nil, errors.Wrap(err, "parse progress")
		}

		return &progress, nil
	}
}

func ParseProgress(rows [][]byte) (Progress, error) {
	progress := Progress{}

	for _, rawRow := range rows {
		if len(rawRow) == 0 {
			continue
		}

		row := strings.Split(string(rawRow), "=")

		key := row[0]
		val := strings.Trim(row[1], " ")

		switch key {
		case ProgressFrame:
			frame, err := strconv.ParseInt(val, 0, 64)
			if err != nil {
				return progress, errors.Wrap(err, "parse frame number")
			}

			progress.Frame = frame

		case ProgressFPS:
			fps, err := strconv.ParseFloat(val, 10)
			if err != nil {
				return progress, errors.Wrap(err, "parse fps")
			}

			progress.FPS = fps

		case ProgressBitrate:
			if val == "N/A" {
				progress.Bitrate = -1
				break
			}

			bitrate, err := strconv.ParseInt(val, 0, 64)
			if err != nil {
				return progress, errors.Wrap(err, "parse bitrate")
			}

			progress.Bitrate = bitrate

		case ProgressTotalSize:
			if val == "N/A" {
				progress.TotalSize = -1
				break
			}

			totalSize, err := strconv.ParseInt(val, 0, 64)
			if err != nil {
				return progress, errors.Wrap(err, "parse total size")
			}

			progress.TotalSize = totalSize

		case ProgressSpeed:
			speed, err := strconv.ParseFloat(strings.Replace(val, "x", "", 1), 32)
			if err != nil {
				return progress, errors.Wrap(err, "parse speed")
			}

			progress.Speed = speed
		case ProgressOutTime:
			outTime, err := ParseFFmpegDuration(val)
			if err != nil {
				return progress, errors.Wrap(err, "parse out time")
			}

			progress.OutTime = outTime
		}
	}

	return progress, nil
}

type Profile struct {
	args []string
}

func (p *Profile) AddParam(key, value string) {
	p.args = append(p.args, key)
	p.args = append(p.args, value)
}

func (p *Profile) EnableParam(key string) {
	p.args = append(p.args, key)
}

func (p *Profile) String() string {
	return strings.Join(p.args, " ")
}

func ParseProfile(p string) (*Profile, error) {
	args := strings.Split(strings.TrimSpace(p), " ")

	if len(args) == 0 {
		return nil, errors.Errorf("empty profile: %s", p)
	}

	return &Profile{
		args: args,
	}, nil
}

func BuildReceiverURL(host string, sid string) (*url.URL, error) {
	u := &url.URL{
		Host:   host,
		Scheme: "http",
		Path:   "/",
	}

	query := u.Query()
	query.Set("sid", sid)

	u.RawQuery = query.Encode()

	return u, nil
}

func ParseFFmpegDuration(rawDuration string) (time.Duration, error) {
	duration := time.Duration(0)

	segments := strings.Split(rawDuration, ":")
	if len(segments) != 3 {
		return 0, errors.New("wrong duration")
	}

	if hours, err := strconv.Atoi(segments[0]); err == nil {
		duration += time.Hour * time.Duration(hours)
	}

	if min, err := strconv.Atoi(segments[1]); err == nil {
		duration += time.Minute * time.Duration(min)
	}

	if seconds, err := strconv.ParseFloat(segments[2], 64); err == nil {
		duration += time.Second * time.Duration(seconds)
	}

	return duration, nil
}
